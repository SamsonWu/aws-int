import { S3Handler } from 'aws-lambda';

import { S3Services } from '../services/s3-services';

export const read: S3Handler = async (event) => {
  // The console log is only for debug purpose
  console.log(JSON.stringify(event));
  try {
    for (const record of event.Records) {
      if (record.eventSource === 'aws:s3' && record.eventName === 'ObjectCreated:Put') {
        const fileName = record.s3.object.key;
        const bucket = record.s3.bucket.name;
        const fileExt = fileName.split('.').pop();
        if (fileExt === 'csv') {
          await S3Services.getFile(bucket, fileName);
        } else {
          console.log(`Skipping the file [${fileName}] as it's not a csv.`);
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
};

S3Services.getFile('aws-int-dev-csv-files', 'test.csv');
