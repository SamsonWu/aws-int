import { parse, ParseResult } from 'papaparse';
import { Location } from '../models/location';
import { S3, SharedIniFileCredentials } from 'aws-sdk';
import { awsConfig } from '../config/aws';
import { isEmpty } from 'lodash';

const sssOptions: S3.ClientConfiguration = {};

// credentials only for local debugging
if (!isEmpty(awsConfig.profile)) {
  console.log(`Creating credentials for profile [${awsConfig.profile}]`);
  sssOptions.credentials = new SharedIniFileCredentials({ profile: awsConfig.profile });
  sssOptions.region = awsConfig.region;
}

const s3 = new S3(sssOptions);

export class S3Services {
  static async getFile(bucket: string, fileName: string) {
    const s3Data = await s3.getObject({
      Bucket: bucket,
      Key: fileName,
    }).promise();

    const promises = await this.extractLocation(s3Data);
    await Promise.all(promises);
  }

  static async extractLocation(data: S3.GetObjectOutput) {
    const promises: Promise<void>[] = [];

    // Papaparse has a way to handle big file, this can be improved by reading S3 object
    // streaming and separates the data by chunks, but now we use the simple way to demo
    return new Promise<Promise<void>[]>((res, rej) => parse<Location>(
      data.Body?.toString(), {
        chunkSize: 1024,
        header: true,
        chunk: results => this.saveLocations(results, promises),
        complete: () => res(promises),
        error: err => rej(err),
      }));
  }

  static saveLocations(results: ParseResult<Location>, promises: Promise<void>[]) {
    for (const row of results.data) {

      console.log(JSON.stringify(row));

      const location = new Location();
      location.latitude = row.latitude;
      location.longitude = row.longitude;
      location.address = row.address;
      promises.push(location.put().catch(err => console.error(err)));
    }
  }
}
