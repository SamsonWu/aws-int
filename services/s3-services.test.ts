import { SinonStub, assert, expectation } from 'sinon';
import { S3, DynamoDB } from 'aws-sdk';

import { loadSandbox } from '../libs/test-helpers';
import { S3Services } from './s3-services';
import { readFileSync } from 'fs';
import { DynamoAccessor } from '../libs/dynamo-accessor';
import { expect } from 'chai';

describe('S3Services', () => {

  describe('getFile', () => {
    let getObjectStub: SinonStub;
    let extractLocationStub: SinonStub;

    loadSandbox((testSandbox) => {
      extractLocationStub = testSandbox.stub(S3Services, 'extractLocation');
      getObjectStub = testSandbox.stub((S3 as any).services['2006-03-01'].prototype, 'getObject');
    });

    it('should get file from s3', async () => {
      getObjectStub.returns({ promise: () => 'csv text' });
      extractLocationStub.returns([]);
      await S3Services.getFile('fakeBucket', 'fakeFile');
      assert.calledOnce(getObjectStub);
      assert.calledOnce(extractLocationStub);
    });
  });

  describe('extractLocation', () => {
    const testData = readFileSync(`${__dirname}/../test-data/test.csv`).toString('utf-8');
    const s3Data = {
      Body: {
        toString: () => testData,
      },
    };

    let saveLocationsStub: SinonStub;

    loadSandbox((testSandbox) => {
      saveLocationsStub = testSandbox.stub(S3Services, 'saveLocations');
    });

    it('should extract 9 locations from csv', async () => {
      await S3Services.extractLocation(s3Data);
      expect(saveLocationsStub.args[0][0].data.length).eq(9);
    });

    it('should extract data with proper columns', async () => {
      await S3Services.extractLocation(s3Data);
      const extractedData = saveLocationsStub.args[0][0].data;
      for (const row of extractedData) {
        expect(row).haveOwnProperty('latitude');
        expect(row).haveOwnProperty('longitude');
        expect(row).haveOwnProperty('address');
      }
    });
  });

  describe('saveLocations', () => {
    const testData = readFileSync(`${__dirname}/../test-data/test.csv`).toString('utf-8');
    const s3Data = {
      Body: {
        toString: () => testData,
      },
    };

    let dynamoPutStub: SinonStub;

    loadSandbox((testSandbox) => {
      dynamoPutStub = testSandbox.stub(DynamoAccessor.prototype, 'put');
    });

    it('should call dynamo with 1 extracted result', () => {
      const promises: Promise<void>[] = [];
      const results = {
        data: [{
          latitude: -43.58299805,
          longitude: 146.89373497,
          address: '840 COCKLE CREEK RD, RECHERCHE TAS 7109',
        }],
      };
      dynamoPutStub.resolves('success');

      S3Services.saveLocations(results as any, promises);
      expect(dynamoPutStub.callCount).eq(1);
    });
  });
});
